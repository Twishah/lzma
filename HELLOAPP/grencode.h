#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include<vector>

#include <iostream>
#include <fstream>
#include "7zFile.h"
#include "7zTypes.h"
#include "LzmaEnc.h"
#include "LzmaDec.h"
#include "Alloc.h"
bool encode(unsigned char* input, size_t length, unsigned char** output, size_t* out_len);
bool decode(char* input, size_t length, unsigned char** output, size_t* out_len);
bool GrEncodeDecode(int num_Args, const char* args[]);
SRes grDecode(CLzmaDec* state, char* inbuff, unsigned char** output, UInt64 unpackSize, size_t inputbufferlength);

#define IN_BUF_SIZE (1 << 16)
#define OUT_BUF_SIZE (1 << 16)

#define UNUSED_VAR(x) (void)x;