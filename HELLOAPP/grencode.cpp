#include "grencode.h"
#include <time.h>

bool encode(unsigned char* input, size_t length, unsigned char** output, size_t* out_len)
{
	LARGE_INTEGER frequency;
	LARGE_INTEGER start;
	LARGE_INTEGER end;
	double interval;

	CLzmaEncHandle enc;
	SRes res;
	CLzmaEncProps props;
	
	enc = LzmaEnc_Create(&g_Alloc);
	if (enc == 0)
		return SZ_ERROR_MEM;
	
	LzmaEncProps_Init(&props);
	res = LzmaEnc_SetProps(enc, &props);
	
	if (res == SZ_OK)
	{
		Byte header[LZMA_PROPS_SIZE + 8];
		size_t headerSize = LZMA_PROPS_SIZE;
		
		res = LzmaEnc_WriteProperties(enc, header, &headerSize);
		for (int i = 0; i < 8; i++)
			header[headerSize++] = (Byte)(length >> (8 * i));
		memcpy(*output, header, LZMA_PROPS_SIZE + 8);
		if (res == SZ_OK) {
			QueryPerformanceFrequency(&frequency);
			QueryPerformanceCounter(&start);
			res = LzmaEnc_MemEncode(enc, (*output) + (LZMA_PROPS_SIZE + 8), out_len, input, length, 0, NULL, &g_Alloc, &g_Alloc);
			QueryPerformanceCounter(&end);
			interval = (double)(end.QuadPart - start.QuadPart) / frequency.QuadPart;
			std::cout << "inside encode" << std::endl;
			printf("%f\n", interval);
		}
			
		*out_len += (LZMA_PROPS_SIZE + 8);

	}
	LzmaEnc_Destroy(enc, &g_Alloc, &g_Alloc);
	return true;
}
inline bool exists_test3(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}
bool decode(char* input, size_t length, unsigned char** output, size_t* out_len)
{
	UInt64 unpackSize;
	CLzmaDec state;
	SRes res;
	char header[LZMA_PROPS_SIZE + 8] = { 0 };
	memcpy(header, input, LZMA_PROPS_SIZE + 8);
	unpackSize = 0;
	for (int i = 0; i < 8; i++)
		unpackSize += (UInt64)header[LZMA_PROPS_SIZE + i] << (i * 8);
	LzmaDec_Construct(&state);
	if (unpackSize > length) {
		*output = new unsigned char[unpackSize];
	}
	

	LzmaDec_Allocate(&state, (unsigned char*)header, LZMA_PROPS_SIZE, &g_Alloc);
	res = grDecode(&state, input + (LZMA_PROPS_SIZE + 8), output, unpackSize, length - (LZMA_PROPS_SIZE + 8));
	*out_len = unpackSize;
	return true;
}

SRes grDecode(CLzmaDec* state, char* inbuff, unsigned char** output, UInt64 unpackSize, size_t inputbufferlength)
{
	
	LzmaDec_Init(state);
	SRes res;
	ELzmaFinishMode finishMode = LZMA_FINISH_ANY;
	ELzmaStatus status;
	SizeT outProcessed = unpackSize;
	SizeT inProcessed = inputbufferlength;
	res = LzmaDec_DecodeToBuf(state, *output, &outProcessed, (unsigned char*)inbuff, &inProcessed, finishMode, &status);
	return res;
	
}

bool GrEncodeDecode(int num_Args, const char* args[])
{
	LARGE_INTEGER frequency;
	LARGE_INTEGER start;
	LARGE_INTEGER end;
	double interval;

	std::cout << args[1] << std::endl;
	if (exists_test3(args[1]))
		std::cout << "file present" << std::endl;
	
	
	std::ifstream file(args[1], std::ios::binary );
	file.seekg(0, file.end);
	long size = file.tellg();
	size_t filesize = size;

	size_t* fileout = &filesize;
	file.seekg(0);
	char* input = new char[size];
	file.read(input, size);
	
		
	unsigned char* output = new unsigned char [size];
	//unsigned char* output ;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	encode((unsigned char*)input, size, &output, fileout);
	//decode(input, size, &output, fileout);
	QueryPerformanceCounter(&end);
	interval = (double)(end.QuadPart - start.QuadPart) / frequency.QuadPart;

	printf("%f\n", interval);

	std::ofstream outfile(args[2], std::ios::binary);

	outfile.write((char*)output, *fileout);
	delete[] input;
	delete[] output;
	
	return true;

}
