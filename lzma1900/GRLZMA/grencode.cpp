#include "grencode.h"


bool encode(unsigned char* input, size_t length, unsigned char** output, size_t* out_len)
{
	
	CLzmaEncHandle enc;
	SRes res;
	CLzmaEncProps props;
	
	enc = LzmaEnc_Create(&g_Alloc);
	if (enc == 0)
		return SZ_ERROR_MEM;
	
	LzmaEncProps_Init(&props);
	res = LzmaEnc_SetProps(enc, &props);
	
	if (res == SZ_OK)
	{
		Byte header[LZMA_PROPS_SIZE + 8];
		size_t headerSize = LZMA_PROPS_SIZE;
		
		res = LzmaEnc_WriteProperties(enc, header, &headerSize);
		for (int i = 0; i < 8; i++)
			header[headerSize++] = (Byte)(length >> (8 * i));
		memcpy(*output, header, LZMA_PROPS_SIZE + 8);
		if (res == SZ_OK) {
			
			res = LzmaEnc_MemEncode(enc, (*output) + (LZMA_PROPS_SIZE + 8), out_len, input, length, 0, NULL, &g_Alloc, &g_Alloc);
			
		}
			
		*out_len += (LZMA_PROPS_SIZE + 8);

	}
	LzmaEnc_Destroy(enc, &g_Alloc, &g_Alloc);
	return true;
}

bool decode(char* input, size_t length, unsigned char** output, size_t* out_len)
{
	UInt64 unpackSize;
	CLzmaDec state;
	SRes res;
	char header[LZMA_PROPS_SIZE + 8] = { 0 };
	memcpy(header, input, LZMA_PROPS_SIZE + 8);
	unpackSize = 0;
	for (int i = 0; i < 8; i++)
		unpackSize += (UInt64)header[LZMA_PROPS_SIZE + i] << (i * 8);
	LzmaDec_Construct(&state);
	if (unpackSize > length) {
		*output = new unsigned char[unpackSize];
	}
	

	LzmaDec_Allocate(&state, (unsigned char*)header, LZMA_PROPS_SIZE, &g_Alloc);
	res = grDecode(&state, input + (LZMA_PROPS_SIZE + 8), output, unpackSize, length - (LZMA_PROPS_SIZE + 8));
	*out_len = unpackSize;
	LzmaDec_Free(&state, &g_Alloc);
	return true;
}

SRes grDecode(CLzmaDec* state, char* inbuff, unsigned char** output, UInt64 unpackSize, size_t inputbufferlength)
{
	
	LzmaDec_Init(state);
	SRes res;
	ELzmaFinishMode finishMode = LZMA_FINISH_ANY;
	ELzmaStatus status;
	SizeT outProcessed = unpackSize;
	SizeT inProcessed = inputbufferlength;
	res = LzmaDec_DecodeToBuf(state, *output, &outProcessed, (unsigned char*)inbuff, &inProcessed, finishMode, &status);
	return res;
	
}

