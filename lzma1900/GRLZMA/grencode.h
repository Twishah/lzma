#pragma once
#define _CRT_SECURE_NO_WARNINGS

#ifdef GRL_EXPORTS
#define GRL_API __declspec(dllexport)
#else
#define GRL_API __declspec(dllimport)
#endif

#include<vector>

#include <iostream>
#include <fstream>
#include "7zFile.h"
#include "7zTypes.h"
#include "LzmaEnc.h"
#include "LzmaDec.h"
#include "Alloc.h"
extern "C"  GRL_API bool encode(unsigned char* input, size_t length, unsigned char** output, size_t* out_len);
extern "C"  GRL_API bool decode(char* input, size_t length, unsigned char** output, size_t* out_len);
SRes grDecode(CLzmaDec* state, char* inbuff, unsigned char** output, UInt64 unpackSize, size_t inputbufferlength);

#define IN_BUF_SIZE (1 << 16)
#define OUT_BUF_SIZE (1 << 16)

#define UNUSED_VAR(x) (void)x;