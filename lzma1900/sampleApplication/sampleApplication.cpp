// sampleApplication.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define _CRTDBG_MAP_ALLOC
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#include<crtdbg.h>

#include <iostream>
#include "grencode.h"
#include <time.h>
#include <fstream>
#include<vector>


int main()
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    LARGE_INTEGER frequency;
    LARGE_INTEGER start;
    LARGE_INTEGER end;
    double interval;
    std::cout << "Hello World!\n";
    //const char* args[] = { "e","UnityCrashHandler64.exe","UnityCrashHandler64_compress.exe" };
   /* const char* args[] = { "d","UnityCrashHandler64_compress.exe","UnityCrashHandler64_deccompress.exe"};*/
    const char* args[] = { "e","00132019587609732837.pgm","vel_compress.lzma" };

    //const char* args[] = { "d","vel_compress.lzma","decompressed_vel.pgm" };
    std::ifstream file(args[1], std::ios::binary);
    file.seekg(0, file.end);
    long size = file.tellg();
    size_t filesize = size;
    size_t* fileout = &filesize;
    file.seekg(0);
    char* input = new char[size];
    file.read(input, size);
    unsigned char* output = new unsigned char[size];
    //unsigned char* output ;

    if (args[0] == "e") {
        QueryPerformanceFrequency(&frequency);
        QueryPerformanceCounter(&start);
        encode((unsigned char*)input, size, &output, fileout);
        QueryPerformanceCounter(&end);
        interval = ((double)(end.QuadPart - start.QuadPart) / frequency.QuadPart) * 1000;

        std::cout << "Duration of encode execution in milli seconds : " << interval << std::endl;

    }
    else {
        QueryPerformanceFrequency(&frequency);
        QueryPerformanceCounter(&start);
        decode(input, size, &output, fileout);
        QueryPerformanceCounter(&end);
        interval = ((double)(end.QuadPart - start.QuadPart) / frequency.QuadPart) * 1000;
        std::cout << "Duration of Decode execution in milli seconds : " << interval << std::endl;
        //printf("%f\n", interval);
    }
    std::ofstream outfile(args[2], std::ios::binary);

    outfile.write((char*)output, *fileout);
    delete[] input;
    delete[] output;
}
